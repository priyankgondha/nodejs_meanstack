// console.log("this is my first file...");
// console.log("This is first step..");
// a = 5;
// b = 5;
// c = a + b;
// console.log("sum:", c);
// arr = [5, 2, 6, 8, 9, 10, 5];
// arr = arr.map((x) => {
//   return x * 10;
// });
// console.log("new ", arr);

// str = "priyank gondha ";
// str2 = "Priyank gondha";
// srt3 = "priyank,nehal,karan,krushik";

// console.log("str length:", str.length);
// console.log("upper:", str.toUpperCase());
// console.log("lower",str.toLowerCase());
// console.log("trim:",str.trim(" "));
// console.log("charAt",str.charAt(1));
// console.log("substring", str.substring(1,8));
// console.log("equals",str==str2);
// console.log("not equals",str!=str2);
// console.log("replace",str.replace("gondha","patel"));
// console.log("trim",str.trim()+"krushik");
// console.log("hello");
// console.log("split",str3.split(','));

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');


var app = express();


const {mongoose} = require('./db.js');
// const addrestcontroller = require('./controller/addrest.js');
const studentcontroller = require('./controller/student_curd.js');
const studentfeescontroller = require('./controller/student_fees.js');
const subjectcontroller = require('./controller/subject_info')


app.use(express.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.json());
app.use(cors({ origin: 'http://localhost:4200' }));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.set("view engine","ejs");


// app.use('/admin',addrestcontroller.router);
app.use('/student',studentcontroller.router);
app.use('/student/account',studentfeescontroller.router)
app.use('/subject',subjectcontroller.router);

var port = 4000
app.listen(port,()=>console.log("Server started at port 4000"));
