const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();

const addrestschema = new mongoose.Schema({
  name: { type: String },
  email: { type: String },
  mobile: { type: String },
  address: { type: String },
  service: { type: String },
});

const StudentSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  id: {
    type: Number,
    unique: true,
    required: true,
  },
  isPass: {
    type: Boolean,
  },
});

// const Restaurent = new mongoose.model("restaurentlists", addrestschema);
// const Student = new mongoose.model("studentdatas", StudentSchema);

router.post("/save/student/data", async (req, res) => {
  console.log(req.body);
  var data = new Student(req.body);
  console.log(data);
  try {
    const dataToSave = await data.save();
    console.log(dataToSave);
    res.status(200).send(dataToSave);
  } catch (error) {
    res.status(400).send({ message: error.message });
  }
});

router.post("/addrest", async (req, res) => {
  console.log("body", req.body);
  var rest = new Restaurent(req.body);
  console.log(`Rest: ${rest}`);
  await rest.save((err, doc) => {
    if (!err) {
      res.send(doc);
      console.log(`Doc: ${doc}`);
    } else {
      console.log("Error in doc save:" + JSON.stringify(err, undefined, 2));
    }
  });
});

router.get("/studentlist", async (req, res) => {
  try {
    console.log("get request for student");
    const studentname = await Student.find();
    res.send(studentname);
    console.log(studentname);
  } catch {
    console.log("Error in get list");
  }
});



router.get("/restlist", async (req, res) => {
  try {
    console.log("get request");
    const restname = await Restaurent.find();
    res.send(restname);
    console.log(restname);
  } catch {
    console.log("Error in get list");
  }
});

router.delete("/restlist/:id", async (req, res) => {
  try {
    console.log("Delete ID:", req.params.id);
    await Restaurent.findOneAndRemove(req.params.id).then((data) => {
      res.send(data);
      console.log(data);
    });
  } catch (error) {
    res.send(err);
  }
});

router.get("/restdata/:id", async (req, res) => {
  console.log("id", req.params.id);
  console.log(typeof req.params.id);
  let data;
  Restaurent.findOne({ _id: req.params.id }, function (err, document) {
    try {
      console.log(document);
      data = document;
      res.send(document);
    } catch (err) {
      console.log(err);
      res.err;
    }
  });
});

router.put("/updaterest/:id", async (req, res) => {
  try {
    console.log(req.body);
    console.log(req.params.id);
    const filter = { _id: req.params.id };
    const updateObj = {
      name: req.body?.name,
      email: req.body?.email,
      mobile: req.body?.mobile,
      address: req.body?.address,
      service: req.body?.service,
    };
    updateRest = await Restaurent.findOneAndUpdate(filter, updateObj);
    await updateRest.save();
    res.send(updateObj);
  } catch (err) {
    console.log("err is coming into update");
    res.send(err);
  }
});

// module.exports = Restaurent;
// module.exports = router;
// module.exports = { Restaurent, router, Student };