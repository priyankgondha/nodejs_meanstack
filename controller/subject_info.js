const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();

const { Student } = require('./student_curd')

const SubjectInfoSchema = new mongoose.Schema({
    sub_name:{
        type: String,
        required: true,
    },
    sub_id:{
        type: Number,
        required: true,
    },
    class:{
        type:Number,
        required: true
    },
    sub_exam_mark:{
        type: Number,
        required: false,
    },
    sub_mid_mark:{
        type: Number,
        required: false,
    },
    year:{
        type: Number,
        default: new Date().getUTCFullYear()
    },
    create_date:{
        type: Date,
        default: new Date()
    },

});

const Subject = new mongoose.model("subjectlists", SubjectInfoSchema);

router.get("/list", async (req, res) => {
    try {
        // const query = Subject.aggregate([
        //     {
        //       $sort: { class: 1 }
        //     }
        //   ]);

        // const query = Subject.aggregate([
        // {
        //     $lookup: {
        //         from:"studentlists",
        //         let: { class: "$class" },
        //         pipeline:[
        //         {
        //             $match: {
        //                 $expr: { $eq: ["$standard", "$$class"] }
        //             }
        //         },
        //         {
        //             $project: { _id: 0, student_name: "$student_name", city: "$city" }
        //         }
        //         ],
        //         as: "result"
        //     }
        // }
        // ]);

        const query = Student.aggregate([
            {
                $lookup:{
                    from:"subjectlists",
                    let:{standard:"$standard"},
                    pipeline:[{
                        $match:{
                            $expr:{ $eq:["$class","$$standard"]}
                        }
                    },
                    {
                        $project: { _id: 0, sub_name: "$sub_name", sub_exam_mark: "$sub_exam_mark", year:"$year" , sub_id: "$sub_id" }
                    }
                ],
                as: "result"
                }
            }
        ]);

        const studentdata = await query;
        res.send(studentdata);
        console.log(studentdata);
    } catch {
        console.log("Error in get list");
    }
});

module.exports = { router };