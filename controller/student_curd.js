const express = require("express");
const mongoose = require("mongoose");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const handlebars = require('handlebars');
const consolidate = require('consolidate');
const fs = require('fs');
const path = require('path');
// import * as fs from 'fs';
// import { join } from 'path';
// import { readFile } from 'fs';

// const { required } = require("nodemon/lib/config");
const app = express();
app.use(express.json());
const router = express.Router();

app.engine('hbs', consolidate.handlebars);
app.set('view engine', 'hbs');
app.set('views', __dirname + '/email-template');

const StudentInfoSchema = new mongoose.Schema({
    student_name: {
      type: String,
      required: true,
      lowercase: true
    },
    student_surname:{
        type: String,
        required: true,
        lowercase: true
    },
    gender:{
        type: String,
        required: true,
        uppercase: true
    },
    dob:{
        type: String,
        required: true,
        lowercase: true
    },
    father_name:{
        type: String,
        lowercase: true
    },
    mother_name:{
        type: String,
        lowercase: true
    },
    email:{
        type: String,
    },
    city:{
        type: String,
    },
    mobile:{
        type: Number,
    },
    isd_code:{
        type: Number,
        required: true,
    },
    addmission_date:{
        type: Date,
        default: new Date()
    },
    standard:{
        type:Number,
        required: true
    },
    class_division:{
        type: String,
        required: true,
    },
    student_id:{
        type: mongoose.Schema.Types.ObjectId,
    }
  });

const Student = new mongoose.model("studentlists", StudentInfoSchema);

const secretKey = '6sPrImyczVikk7yZ_p12L2l3lUyR-ZCt9k1HvRAr1gU';



    
router.post("/save/student", async (req, res) => {
    const data = new Student({student_id: new mongoose.Types.ObjectId(),...req.body});
    try {
      const dataToSave = await data.save();
      res.status(200).send("Student Data Saved Successfully");
    } catch (error) {
      res.status(400).send({ message: error.message });
    }
});

router.put("/update/student/:student_id", async (req, res) =>{
    try{
        const validProperties = Object.keys(Student.schema.paths);
        const providedProperties = Object.keys(req.body);
        const invalidProperties = providedProperties.filter(prop => !validProperties.includes(prop));

        if (invalidProperties.length > 0) {
            return res.status(400).send(`Invalid properties: ${invalidProperties.join(', ')}`);
        }

        const filter  = {student_id: req.params.student_id};
        const newData  = {$set:req.body}
        const updateStudent = await Student.findOneAndUpdate(filter,newData, { new: true });
       
        if (!updateStudent) {
            return res.status(404).send("Student not found");
        }else{
            return res.send("Stundent's Id valid and updated");
        }
    }catch (error) {
        console.log("Invalid student id");
    }
});




router.post("/save/student-send-email", async (req, res) => {
    const data = new Student({student_id: new mongoose.Types.ObjectId(),...req.body});
    try {
      const dataToSave = await data.save();
      const transporter = nodemailer.createTransport({
        host: 'sandbox.smtp.mailtrap.io',
        port: 2525,
        secure: false,
        auth: {
          user: 'bfea001312918e',
          pass: '1c45e08c979520',
        },
      });
      const userName = dataToSave.student_name +' '+ dataToSave.student_surname;

      const mailOptions = {
        from: 'priyankgondha555@gmail.com',
        to: 'priyank.gondha.sa@gmail.com',
        subject: 'User name',
        html: await renderTemplate('welcome.hbs', { userName }),
      };
  
      const info = await transporter.sendMail(mailOptions);
      console.log('===>>',info);
      console.log(info.messageId);



      res.status(200).send("Student Data Saved Successfully");
    } catch (error) {
      res.status(400).send({ message: error.message });
    }

    async function renderTemplate(templateName, context) {
      const templatePath = path.join(__dirname,`../email-template/${templateName}`);
      const templateContent = await fs.promises.readFile(templatePath,'utf-8', (err, data) => {
        if(err){
          console.log('err',err);
        }
        else{
          return data;
        }
      });
      const compiledTemplate = handlebars.compile(templateContent);
      return compiledTemplate(context);
    }
});

router.post('/login', async (req, res) => {
    const { username, password ,email} = req.body;
    console.log(req.body)
    const isValidUser = await verifyUser(username, password, email);
    if (isValidUser) {
      const userData = await Student.findOne({ email });
      const token = jwt.sign({ userData }, secretKey, { expiresIn: '1h' });
      res.json({ token });
    } else {
      res.status(401).json({ message: 'Invalid credentials' });
    }
});

router.get('/protected', verifyToken, (req, res) => {
    res.json({ message: 'Protected route accessed successfully' });
});

function verifyToken(req, res, next) {
    const token = req.headers.authorization;
    if (token) {
      jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
          return res.status(403).json({ message: 'Invalid token' });
        }
        req.user = decoded;
        next();
      });
    } else {
      res.status(401).json({ message: 'Token not provided' });
    }
}

async function verifyUser(username, password, email) {
    try {
        const user = await Student.findOne({ email });
        if(user){
            const hashedPassword = await bcrypt.hash(user.email, 10); 
            return bcrypt.compare(email, hashedPassword);
        }
    } catch (error) {
        console.error(error);
    }
}

module.exports = { router, Student: Student };