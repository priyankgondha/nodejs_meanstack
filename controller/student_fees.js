const express = require("express");
const ExcelJS = require('exceljs');
const mongoose = require("mongoose");
const router = express.Router();

const { Student } = require('./student_curd')


const StudentFeesInfoSchema = new mongoose.Schema({
    student_id:{
        type: String,
        required: true,
    },
    student_email:{
        type: String,
        required: true,
    },
    standard:{
        type:Number,
        required: false
    },
    class_division:{
        type: String,
        required: false,
    },
    fees_type:{
        type: String,
        required: true,
    },
    amount:{
        type:Number,
        required:true
    },
    payment_date:{
        type: Date,
        default: new Date()
    },
    payment_status:{
        type: String,
        required: false
    },
    slip_no:{
        type: mongoose.Schema.Types.ObjectId,
    },
});

const StudentFees = new mongoose.model("studentfeeslists", StudentFeesInfoSchema);


router.post("/payfees", async(req,res)=>{
    const data = new StudentFees({slip_no: new mongoose.Types.ObjectId(),...req.body});
    try {
      const dataToSave = await data.save();
      const update = await updatePaymenyStatus(dataToSave.slip_no, req.body.student_id);
      if(update){
          res.status(200).send("Payment Transaction Successful");
      }
    } catch (error) {
      res.status(400).send({ message: error.message });
    }
});


async function updatePaymenyStatus(slip_no,student_id){
    try {
        const stundentInfo = await Student.findOne({student_id});
        const updatedPaymentStatus = await StudentFees.findOneAndUpdate(
            { slip_no: slip_no },
            { $set: {payment_status:'Successful', class_division:stundentInfo.class_division , standard: stundentInfo.standard}},
            { new: true }
        );
        if (!updatedPaymentStatus) {
            return "Payment transaction failed";
        }
        return true;
    } catch (error) {
        return error
    }
};

router.get("/total", async (req, res) => {
    try {
        // const query = Student.aggregate([
        //     { $match: { $or: [{ email: "shivanijavia@gmail.com" },{ email: "liam.clark@example.com" }]  }},
        //     { $group: { _id: "$standard", students: { $push: "$$ROOT" } , name:{ $push:"$student_name"} } },
        //     {
        //         $addFields:
        //           {
        //             count : { $size: "$students" }
        //           }
        //     }
        //   ]);


        // const query = StudentFees.aggregate( [
        //     {
        //       $lookup:
        //         {
        //           from: "studentlists",
        //           localField: "student_id",
        //           foreignField: "student_id",
        //           as: "arr"
        //         }
        //    },
        // //    { $match: { $or: [{ fees_type: "examfee" }]  }},
        //  ] )


        const query = StudentFees.aggregate([
            {
              $lookup: {
                from: "studentlists", // Ensure this matches the actual collection name in your database
                let: { student_email: "$student_email" },
                pipeline: [
                  {
                    $match: {
                      $expr: { $eq: ["$email", "$$student_email"] }
                    }
                  },
                  {
                    $project: { _id: 0, student_name: "$student_name", city: "$city", }
                  }
                ],
                as: "arr"
              }
            }
          ]);
      const studentdata = await query;
      res.send(studentdata);
      console.log(studentdata);
    } catch {
      console.log("Error in get list");
    }
  });


  router.get("/total-excel", async (req, res) => {
    try{
      const query2 = await StudentFees.find({});

      const workbook = new ExcelJS.Workbook();
      const worksheet = workbook.addWorksheet('Sheet 1');

      worksheet.columns = [
        { header: 'Student ID', key: 'student_id' },
        { header: 'Email', key: 'student_email' },
        { header: 'Standard', key: 'standard' },
        { header: 'Fee Type', key: 'fees_type' },
        { header: 'Amount', key: 'amount' },
      ];

      worksheet.addRows(query2);

      res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      res.setHeader('Content-Disposition', 'attachment; filename=example.xlsx');
      await workbook.xlsx.write(res);

      res.end();

      // res.send({total:query2});
    }catch(e){
      console.log("Error in get list",e);
    }
  })


module.exports = { router };