const mongoose = require('mongoose');
mongoose.set('strictQuery', false);
mongoose.connect('mongodb://localhost:27017/school_management_db', { 
    useNewUrlParser: true, 
    useUnifiedTopology: true 
}, (err) => {
    if (!err) {
        console.log("MongoDB Connection is succeeded");
    } else {
        console.log("Error in DB Connection: " + JSON.stringify(err, undefined, 2));
    }
});
module.exports= mongoose;